FROM pypy:3.6

EXPOSE 8080

RUN apt-get update && apt-get install -y libev-dev
RUN pip install bjoern

RUN mkdir /app

COPY ./dist/fastbird-0.0.1-py3-none-any.whl /app
COPY wsgi.py /app

RUN pip install /app/fastbird-0.0.1-py3-none-any.whl

CMD ["pypy3", "/app/wsgi.py"]
