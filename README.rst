########
FASTBIRD
########

It's fast, I think


Integration
===========

Make sure the tests are green

.. code-block:: shell

    tox

Make sure the code is nice

.. code-block:: shell

    black src test

Make a fresh *wheel*

.. code-block:: shell

    python setup.py bdist_wheel

Build an image

.. code-block:: shell

    docker build -t fastbird .

Run it

.. code-block:: shell

    docker run -p 8080:8080 fastbird

If something goes wrong you might want to kill per process still
listening on port

.. code-block:: shell

    lsof -ti tcp:8080 | xargs kill


Benchmarks
==========


CPU Load
--------

.. code-block:: none

    $ siege -b -c 100 -t 2M https://localhost:8080/synccpu/1

Slow I/O
--------

.. code-block:: none

    $ siege -b -c 100 -t 2M https://localhost:8080/syncio/1


Slow I/O with asyncio
---------------------

.. code-block:: none

    $ siege -b -c 100 -t 2M https://localhost:8080/asyncio/1
