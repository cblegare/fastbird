import signal

import bjoern
from fastbird.app import create_app


def receive_signal(signal_number, frame):
    print('Received:', signal_number)
    return


def register_handled_signals():
    signal.signal(signal.SIGHUP, receive_signal)
    signal.signal(signal.SIGINT, receive_signal)
    signal.signal(signal.SIGQUIT, receive_signal)
    signal.signal(signal.SIGILL, receive_signal)
    signal.signal(signal.SIGTRAP, receive_signal)
    signal.signal(signal.SIGABRT, receive_signal)
    signal.signal(signal.SIGBUS, receive_signal)
    signal.signal(signal.SIGFPE, receive_signal)
    # signal.signal(signal.SIGKILL, receive_signal)
    signal.signal(signal.SIGUSR1, receive_signal)
    signal.signal(signal.SIGSEGV, receive_signal)
    signal.signal(signal.SIGUSR2, receive_signal)
    signal.signal(signal.SIGPIPE, receive_signal)
    signal.signal(signal.SIGALRM, receive_signal)
    signal.signal(signal.SIGTERM, receive_signal)


if __name__ == "__main__":
    print('Starting bjoern on port 8080...', flush=True)

    register_handled_signals()

    bjoern.run(
        wsgi_app=create_app(),
        host='0.0.0.0',
        port=8080,
        reuse_port=True
    )
