import falcon.testing  # type: ignore

import pytest

from fastbird.app import create_app


@pytest.fixture
def client(app):
    return falcon.testing.TestClient(app)


@pytest.fixture
def app():
    return create_app()


def test_list_images(client):
    doc = {"Hello": "World!"}

    response = client.simulate_get("/")
    result_doc = response.json

    assert result_doc == doc
    assert response.status == falcon.HTTP_OK
