import asyncio
import time

from falcon import Request, Response


class Benchmark(object):
    def on_get(
        self,
        request: Request,
        response: Response,
        load_type: str,
        seconds: int,
    ):
        load_types = {
            "syncio": _sync_slow_io,
            "asyncio": _gathered_async_slow_io,
            "synccpu": _sync_cpu,
        }

        load_types[load_type](int(seconds))


def _sync_slow_io(n: int):
    time.sleep(n)


def _gathered_async_slow_io(n: int):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    gathered = asyncio.gather(_async_slow_io(n), _async_slow_io(n))
    loop.run_until_complete(gathered)
    loop.close()


async def _async_slow_io(n: int):
    await asyncio.sleep(n)


def _sync_cpu(n: int):
    start = time.time()
    time.clock()
    elapsed = 0
    while elapsed < n:
        elapsed = time.time() - start
        _ = n * n
