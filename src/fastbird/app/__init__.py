from typing import List

import falcon  # type: ignore

from fastbird.app.cfg import Config
from fastbird.bench import Benchmark
from fastbird.falcon.middleware import FalconComponent


def register_resources(application: falcon.API) -> falcon.API:
    application.add_route("/{load_type}/{seconds:int}", Benchmark())

    return application


def create_app(**environment_override: str) -> falcon.API:
    configuration = Config(**environment_override)

    application = falcon.API(middleware=collect_middleware(configuration))

    application = register_resources(application)

    return application


def collect_middleware(configuration: Config) -> List[FalconComponent]:
    return []
